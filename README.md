# Mobgen Coruña Leagues for iOS

iOS app for the foosball leagues at Mobgen, so you can keep track of your team.

----------------

  1. If you want to contribute: **pull request**.
  
  1. If you find a bug: Open issue or better **pull request**.
  
  1. If you'd like to request a feature: Open an issue, or even better **pull request**.

**NOTE:** All pull requests to the **develop** branch