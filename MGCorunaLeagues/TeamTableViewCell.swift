//
//  TeamTableViewCell.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/7/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

class TeamTableViewCell : UITableViewCell {
    
    @IBOutlet weak private var marker : UIView?
    @IBOutlet weak private var separator : UIView?
    
    @IBOutlet weak private var frontPlayer : UILabel?
    @IBOutlet weak private var backPlayer : UILabel?
    
    @IBOutlet weak private var points : UILabel?
    @IBOutlet weak private var gamesPlayed : UILabel?
    @IBOutlet weak private var gamesWon : UILabel?
    @IBOutlet weak private var gamesTied : UILabel?
    @IBOutlet weak private var gamesLost : UILabel?
    @IBOutlet weak private var goalDifference : UILabel?
    
    func configure(withTeam team : Team, leagueConditions : League.Conditions, teamsInLeague : Int) {
        
        frontPlayer?.text = team.frontPlayer
        backPlayer?.text = team.backPlayer
        
        points?.text = team.points
        gamesPlayed?.text = team.gamesPlayed
        gamesWon?.text = team.wonGames
        gamesTied?.text = team.drawGames
        gamesLost?.text = team.lostGames
        goalDifference?.text = team.goalsDifference
        
        guard var pos = Int(team.position) else {
            
            marker?.backgroundColor = UIColor.lightGrayColor()
            return
        }
        pos = pos - 1
        
        if pos < leagueConditions.greenZonePositions {
            
            marker?.backgroundColor = UIColor.emeraldGreenColor()
            contentView.backgroundColor = UIColor.emeraldGreenColor().colorWithAlphaComponent(0.3)
        }
        else if pos < leagueConditions.greenZonePositions + leagueConditions.blueZonePositions {
            
            marker?.backgroundColor = UIColor.peterRiverColor()
            contentView.backgroundColor = UIColor.peterRiverColor().colorWithAlphaComponent(0.3)
        }
        else if pos >= teamsInLeague - leagueConditions.redZonePosition {
            
            marker?.backgroundColor = UIColor.pomegranateColor()
            contentView.backgroundColor = UIColor.pomegranateColor().colorWithAlphaComponent(0.2)
        }
        else {
            
            marker?.backgroundColor = UIColor.silverColor()
            contentView.backgroundColor = UIColor.silverColor().colorWithAlphaComponent(0.1)
        }
        
        separator?.hidden = pos == teamsInLeague - 1
    }
}
