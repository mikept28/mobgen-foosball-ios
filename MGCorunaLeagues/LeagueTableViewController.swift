//
//  TableViewController.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import UIKit

class LeagueTableViewController : IndexedViewController {
    
    @IBOutlet private weak var tableView : UITableView?
    
    private var displayedCells : Int = 0
    
    private var league : League!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.alpha = 0
    }
    
    func setLeague(league : League) {

        self.league = league
        title = league.name
    }
    
    private func animatableCells() -> Int {
        
        let cellsThatFit = Int( ceil(CGRectGetHeight((tableView?.bounds)!) / (tableView?.rowHeight)!))
        if cellsThatFit < league.teams.count {
            return cellsThatFit
        }
        
        return league.teams.count
    }
}

//MARK: Table View DataSource

extension LeagueTableViewController : UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = league.teams.count ?? 0
        if count > 0 && tableView.alpha == 0 {
            
            UIView.animateWithDuration(0.3, animations: {
                tableView.alpha = 1
            })
        }
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(String(TeamTableViewCell)) as! TeamTableViewCell
        
        cell.configure(withTeam: league.teams[indexPath.row], leagueConditions: league.conditions, teamsInLeague: league.teams.count)
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row < displayedCells || indexPath.row >= animatableCells() {
            return
        }
        
        cell.contentView.transform = CGAffineTransformMakeScale(2, 2)
        cell.contentView.alpha = 0
        
        UIView.animateWithDuration(0.3, delay: Double(indexPath.row) * 0.05, options: .CurveEaseOut, animations: {
            cell.contentView.transform = CGAffineTransformIdentity
            cell.contentView.alpha = 1
            }, completion: nil)
        
        displayedCells += 1
    }
}

//MARK: Table View Delegate

extension LeagueTableViewController : UITableViewDelegate {
    
    
}