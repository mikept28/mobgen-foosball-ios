//
//  LeagueBuilder.swift
//  MGCorunaLeagues
//
//  Created by Mike Pesate on 6/3/16.
//  Copyright © 2016 MOBGEN. All rights reserved.
//

import Foundation

class LeagueBuilder {
    
    private let kLeaguesNode = "leagues"
    
    func buildLeagues(fromJSON json: NSDictionary?) -> [League]? {
        
        guard let json = json else {
            return nil
        }
        
        guard let leaguesJSON = json[kLeaguesNode] as? NSArray else {
            return nil
        }
        
        var leagues = [League]()
        for (idx, item) in leaguesJSON.enumerate() {
            
            var conditions = League.Conditions(greenZonePositions: 2, blueZonePositions: 0, redZonePosition: 2)
            if idx == 0 {
                
                conditions = League.Conditions(greenZonePositions: 1, blueZonePositions: 0, redZonePosition: 3)
            }
            else if idx == 1{
                
                conditions = League.Conditions(greenZonePositions: 2, blueZonePositions: 2, redZonePosition: 0)
            }
            
            if let league = League.init(withJSON: item as! NSDictionary, conditions: conditions) {
                leagues.append(league)
            }
        }
    
        return leagues
        
    }
    
}